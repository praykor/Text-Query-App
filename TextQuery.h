#ifndef TEXTQUERY_H
#define TEXTQUERY_H
#include <string>
#include <vector>
#include <set>
#include <map>
#include <memory>
#include <fstream>
#include <sstream>
#include <iostream>
#include "QueryResult.h"

//when this program reads input, remember lines in which words appear
//read input a line at a time and break it into parts

class TextQuery {
public:
    typedef std::map<std::string, std::set<int>> wordsContainer;
    typedef std::vector<std::string> wordLinesContainer;
    ////default constructor
    TextQuery(const std::string fileName);

    //function to perform a query
    void query(const std::string, QueryResult &qr);

    //shared pointers to the content,
    //shared pointers cause otherwise sharing the containers is expensive
    //n we are not made of bits
   std::shared_ptr<wordsContainer> getWordPositions() const;
   std::shared_ptr<wordLinesContainer> getWordLines() const;
private:
    std::vector<std::string> wordLines;
    wordsContainer wordPositions;

    //private util functions
    unsigned int sizeOfSet(const std::string searchItem);
};
#endif /* TEXTQUERY_H */

