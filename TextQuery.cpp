#include "TextQuery.h"


typedef std::map<std::string, std::set<int>> wordsContainer;
typedef std::vector<std::string> wordLinesContainer;

TextQuery::TextQuery(std::string fileName) {
    //read the lines of text from the file and add them to the wordLines vector
    std::ifstream in(fileName);
    std::string currLine;
    std::string currWord;
    while (getline(in, currLine)) {
        wordLines.push_back(currLine);
    }

    int lineTracker = 1;
    //now take each line and load it into maps<string set<int>>
    for (auto wordIter = wordLines.begin(); wordIter != wordLines.end(); ++wordIter) {

        std::istringstream in(*wordIter);
        std::string currWord;

        //while we have words in our stringstream
        while (in >> currWord && !in.eof()) {
            //insert them into the word positions 
            if (wordPositions.find(currWord) == wordPositions.end())
                wordPositions.insert({currWord,
                    {lineTracker}});
            else
                wordPositions.at(currWord).insert(lineTracker);
        }
        //increment the line tracker
        ++lineTracker;
    }
    
    in.close();//close file stream
}//end textQuery constructor

//function to query whether the string is there or not
void TextQuery::query(const std::string searchItem, QueryResult &qr) {

    //load the word, wordLines, and word positions into queryResult qr
    qr.setWord(searchItem);
    qr.setwordLines(getWordLines());
    qr.setWordsPositions((getWordPositions()));
    
    //if sizeOfSet is not zero, ie its there
    if (sizeOfSet(searchItem) != 0)
        qr.setWordCount(sizeOfSet(searchItem));
}//end query

//function to return a shared pointer to wordPositions
//caller must check that it isnt null before usage
//the item we are working on is const, ie read only
std::shared_ptr<wordsContainer> TextQuery::getWordPositions() const {
    if (!wordPositions.empty())
        return std::make_shared<wordsContainer>(wordPositions);
    else
        return std::make_shared<wordsContainer>() = nullptr;
}//end wordPositions

//function to return a shared pointer to wordLinePositions
//caller must check that it int null before usage
//wordLines is const
std::shared_ptr<wordLinesContainer> TextQuery::getWordLines() const{
    if(!wordLines.empty())
        return std::make_shared<wordLinesContainer>(wordLines);
    else
        return std::make_shared<wordLinesContainer>() = nullptr;
}//end line positions

//utility function to return the size of the set if word is present
unsigned int TextQuery::sizeOfSet(const std::string searchItem) {
    //if the word is there, return the size of the set
    if (wordPositions.find(searchItem) != wordPositions.end())
        return wordPositions.at(searchItem).size();
    else//otherwise return zero
        return 0;
}
