
#include "TextQuery.h"
#include "QueryResult.h"

/*
 * 
 */
int main() {
    char enterQuery;
    std::string searchTerm;
    std::cout << "Do you wish to search for an item? Y o N\n";
    
    ///while they want to search
    if(std::cin >> enterQuery) {
        enterQuery = std::toupper(enterQuery);
        if(enterQuery == 'Y'){
            
            std::cout << "Please enter the search term: ";
            std::cin >> searchTerm;
            //create a textQuery and initalize the content from chat.txt
            TextQuery tx("Chat.txt");
            
            //create a query result to hold the content
            QueryResult qr;
            
            
            //perform a query for Mystic.
            tx.query(searchTerm, qr);
            
            //print out the current information
            qr.print(std::cout);
            
        }//end if Yes
        else
            std::cout << enterQuery << " entered! Exiting program!";
    }
    
    return 0;
}

