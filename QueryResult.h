#ifndef QUERYRESULT_H
#define QUERYRESULT_H
/*
 * when generating output:
 * program must fetch line numbers associated with particular word
 * line numbers for the word must appear in ascending order no duplicates
 * program must print the text appearing in the input file at a given line num
 * 
 */

/*
 * is the word present
 * how often does it occur
 * line numbers it occurs on
 * corresponding text for each of those line numbers
 */
#include <set>
#include <map>
#include <string>
#include <memory>
#include <iostream>
#include <sstream>
#include <vector>
#include <stdexcept>
#include <algorithm>
#include <iomanip>

class QueryResult {
public:
    typedef std::map<std::string, std::set<int>> wordsContainer;
    typedef std::vector<std::string> linesContainer;
    std::ostream& print(std::ostream &);//prints results of the query
    
    void setWordCount(unsigned int);
    void setWord(const std::string word);
    
    //set the word shared ptrWords positions to the pointer in the func
    void setWordsPositions(std::shared_ptr<wordsContainer>);
    
    void setwordLines(std::shared_ptr<linesContainer>);
private:
    std::shared_ptr<wordsContainer> wordPositions;
    std::shared_ptr<linesContainer> wordLines;
    std::shared_ptr<std::vector<std::string>> matchingLines;
    unsigned int wordCount = 0;
    std::set<int> lineNumbers;
    std::string word;
    
    //utility function to get the lines the word appears on
    std::shared_ptr<std::vector<std::string>> findMatchingLines();
    
};

#endif /* QUERYRESULT_H */

