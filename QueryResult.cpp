
/*
 * when generating output:
 * program must fetch line numbers associated with particular word
 * line numbers for the word must appear in ascending order no duplicates
 * program must print the text appearing in the input file at a given line num
 * 
 */
#include "QueryResult.h"


typedef std::map<std::string, std::set<int>> wordsContainer;

//function to print out everything in formatted manner
std::ostream& QueryResult::print(std::ostream& cout){
    if (wordCount != 0) {//check if the word is there
        std::cout << "Search Term: " << word << " Appears: " << wordCount << " times\n";
        std::cout << "Lines it appears on: \n";

        //take the shared pointer to the vector containing matching lines
        std::shared_ptr<std::vector < std::string>> linePtr = findMatchingLines();

        auto wordPosIter = wordPositions.get()->at(word);
        auto lineNumIter = wordPosIter.cbegin();
        //iterate over the matching lines and print them out
        for (auto linePtrIter = linePtr.get()->begin(); linePtrIter != linePtr.get()->end(); ++linePtrIter) {
            std::cout << "(Line Number: " << std::setw(2) << *lineNumIter << " )" << *linePtrIter << " \n";
            ++lineNumIter;
        }//end for
    }//end if
    else{
        std::cerr << "ERR! Word not present!\n";
    }
    
    
    return cout;
}

//set the wordcount for the item
//must check that the word is present
void QueryResult::setWordCount(unsigned int count){
    if(count != 0)
        wordCount = count;
    else 
        wordCount = 0;
}

void QueryResult::setWord(const std::string searchWord){
    word = searchWord;
}

//function meant to set the pointer to wordsPositions
void QueryResult::setWordsPositions(std::shared_ptr<wordsContainer> wordPos){
    //first check if wordPos is null, because it might be!
    if(wordPos != nullptr)
        wordPositions = wordPos;
    else{
        std::cerr << "ERROR no content in word positions!\n EXITING.";
        throw std::invalid_argument("ERROR no content in word positions\n! EXITING.");
    }
       
}

//setter for wordLines
void QueryResult::setwordLines(std::shared_ptr<linesContainer> lineCont){
    //first check if lineCont is null, it might be!
    if(lineCont != nullptr)
        wordLines = lineCont;
    else{
        std::cerr << "ERR no content in lines container!\n EXITING.";
        throw std::invalid_argument("ERROR no content in word positions\n! EXITING.");
    }
}

//function to find the matching lines
//caller must make sure this isnt null
std::shared_ptr<std::vector<std::string>> QueryResult::findMatchingLines(){
    std::vector<std::string> matchingLines;
    
    //iterate over each line in vector
    for(auto lineIter = wordLines.get()->begin(); lineIter != wordLines.get()->end(); ++lineIter){
        //iterate over each word in the line
        //we use a stringstream so we can use the line as a stream
        std::stringstream in(*lineIter);
        std::string currWord;
        
        //continously read from in
        while(in >> currWord && in.good()){
            //if the word we are on matches the word we want 
            if(currWord == word)
                //load it in
                matchingLines.push_back(*lineIter);
        }//stringstream while
            
    }//end for line iterator
    
    //return a shared ptr to matching lines
    return std::make_shared<std::vector<std::string>>(matchingLines);
}//end findMatchingLines
